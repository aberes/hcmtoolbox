Welcome to the HCM custom tool library. 

Developed by the Automation Group at HCM.

This effort reflects our desire to streamline workflows and generate in house solutions to technological bottle necks.
As a task force within the Design Technology Committee, the Automation Group leverages computational logics to solve Architectural problems.

All work is to be considered R&D and user discretion is advised!